﻿using System;
using UnityEngine;

public class NFCPlugin : MonoBehaviour
{
    [SerializeField] Page AlertNoFunds;
    [SerializeField] Page AlertValidGate;
    [SerializeField] Page AlertInvalidGate;
    void Start()
    {
        ScanNFC(gameObject.name, "OnFinishScan");
        disableNFCScan();
    }

    void OnFinishScan(string result)
    {
        disableNFCScan();
        Debug.Log("NFC scan callback result: " + result);

        if (result.Substring(0, 3) == "SKM")
        {
            int.TryParse(result.Substring(3), out int ID);
            Debug.Log("gate ID: " + ID);
            if (App.AppSnapshot.Child("SKMGates").HasChild(ID.ToString()))
            {
                string name = App.AppSnapshot.Child("SKMGates").Child(ID.ToString()).Child("name").Value.ToString();

                if (App.LastgateID == 0)
                {
                    SKMGatesList gate = new SKMGatesList(App.AppSnapshot.Child("SKMGates").Child(ID.ToString()).Child("name").Value.ToString(), DateTime.Now.ToString(), "Wejście");
                    string json = JsonUtility.ToJson(gate);
                    App.Reference.Child(App.User.UserId).Child("currentGate").SetValueAsync(ID);
                    App.Reference.Child(App.User.UserId).Child("gateHistory").Child(App.DatetoIDConverter(gate.date)).SetRawJsonValueAsync(json);
                    ValidGate();
                }
                else
                {
                    if (App.LastgateID == ID)
                    {
                        SKMGatesList gate = new SKMGatesList(App.AppSnapshot.Child("SKMGates").Child(ID.ToString()).Child("name").Value.ToString(), DateTime.Now.ToString(), "Wyjście");
                        string json = JsonUtility.ToJson(gate);
                        App.Reference.Child(App.User.UserId).Child("currentGate").SetValueAsync(0);
                        App.Reference.Child(App.User.UserId).Child("gateHistory").Child(App.DatetoIDConverter(gate.date)).SetRawJsonValueAsync(json);
                        ValidGate();
                    }
                    else
                    {
                        double.TryParse(App.AppSnapshot.Child("SKMGates").Child(ID.ToString()).Child("price").Child(App.LastgateID.ToString()).Value.ToString(), out double price);
                        price = Math.Round(price, 2);
                        Debug.Log("gate travel price: " + price);

                        if (App.PassExpireDate > DateTime.Now)
                        {
                            SKMGatesList gate = new SKMGatesList(name, DateTime.Now.ToString(), "Przepustka");
                            string jsongate = JsonUtility.ToJson(gate);

                            App.Reference.Child(App.User.UserId).Child("currentGate").SetValueAsync(0);
                            App.Reference.Child(App.User.UserId).Child("gateHistory").Child(App.DatetoIDConverter(gate.date)).SetRawJsonValueAsync(jsongate);
                            ValidGate();
                        }
                        else if (App.CurrentBalance >= price)
                        {
                            SKMGatesList gate = new SKMGatesList(name, DateTime.Now.ToString(), "- " + string.Format("{0:C2} zł", price).Substring(1));
                            string jsongate = JsonUtility.ToJson(gate);
                            PaymentHistoryList history = new PaymentHistoryList("Wyjście w " + name, gate.date, "- " + string.Format("{0:C2} zł", price).Substring(1));
                            string jsonhistory = JsonUtility.ToJson(history);

                            App.Reference.Child(App.User.UserId).Child("balance").SetValueAsync(string.Format("{0:C2}", App.CurrentBalance - price).Substring(1));
                            App.Reference.Child(App.User.UserId).Child("currentGate").SetValueAsync(0);
                            App.Reference.Child(App.User.UserId).Child("gateHistory").Child(App.DatetoIDConverter(gate.date)).SetRawJsonValueAsync(jsongate);
                            App.Reference.Child(App.User.UserId).Child("paymentHistory").Child(App.DatetoIDConverter(history.date)).SetRawJsonValueAsync(jsonhistory);
                            ValidGate();
                        }
                        else
                        {
                            AlertNoFunds.Load();
                            AlertNoFunds.gameObject.SetActive(true);
                            return;
                        }

                    }
                }
            }
            else
            {
                InvalidGate();
                Debug.LogError("Wrong NFC gate, error!: " + result);
                return;
            }
        }
        else
        {
            InvalidGate();
            Debug.LogError("Wrong NFC tag, error!: " + result);
            return;
        }
    }

    public static void enableNFCScan()
    {
#if UNITY_ANDROID && !UNITY_EDITOR
		AndroidJavaClass javaClass = new AndroidJavaClass("com.unity3d.player.UnityPlayer"); 
		AndroidJavaObject javaObject = javaClass.GetStatic<AndroidJavaObject>("currentActivity"); 
		javaObject.Call("NFCModeOn");
#endif
    }

    public static void disableNFCScan()
    {
#if UNITY_ANDROID && !UNITY_EDITOR
		AndroidJavaClass javaClass = new AndroidJavaClass("com.unity3d.player.UnityPlayer"); 
		AndroidJavaObject javaObject = javaClass.GetStatic<AndroidJavaObject>("currentActivity"); 
		javaObject.Call("NFCModeOff");
#endif
    }

    public static void ScanNFC(string objectName, string functionName)
    {
        switch (Application.platform)
        {
            case RuntimePlatform.Android:
                AndroidJavaClass javaClass = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
                AndroidJavaObject javaObject = javaClass.GetStatic<AndroidJavaObject>("currentActivity");
                javaObject.Call("scanNFClinkup", objectName, functionName);
                break;

            default:
                GameObject.Find(objectName).SendMessage(functionName, "OS_NOT_SUPPORTED");
                break;
        }
    }
    private void ValidGate()
    {
        AlertValidGate.Load();
        AlertValidGate.gameObject.SetActive(true);
    }
    private void InvalidGate()
    {
        AlertInvalidGate.Load();
        AlertInvalidGate.gameObject.SetActive(true);
    }
}