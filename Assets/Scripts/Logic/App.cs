﻿using Firebase;
using Firebase.Auth;
using Firebase.Database;
using Firebase.Unity.Editor;
using Google;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.UI;

public class App : MonoBehaviour
{
    public static FirebaseAuth Auth { get; set; }
    public static PhoneAuthProvider PhoneProvider { get; set; }
    public static FirebaseUser User { get; set; }
    public static DataSnapshot Snapshot { get; set; } = null;
    public static DataSnapshot AppSnapshot { get; set; } = null;
    public static string Email { get; set; } = "";
    public static string Password { get; set; } = "";
    public static string Passwordconfirm { get; set; } = "";
    public static string Phone { get; set; } = "";
    public static string VerificationId { get; set; } = "";
    public static string VerificationCode { get; set; } = "";
    public static DatabaseReference Reference { get; set; }
    public static double CurrentBalance { get; set; }
    public static int LastgateID { get; set; } = 0;
    public static DateTime PassExpireDate { get; set; }

    public static List<SKMGatesList> gates = new List<SKMGatesList>();
    public static List<PaymentHistoryList> payhistory = new List<PaymentHistoryList>();
    public static List<SKMPassList> passes = new List<SKMPassList>();

    public event EventHandler OnLoggedIn;
    public event EventHandler OnGoogleLogIn;
    public event EventHandler OnNewGatesHistory;
    public event EventHandler OnNewHistoryInput;
    public event EventHandler OnReadyUpPhoneRegister;
    public event EventHandler OnBalanceChange;
    public event EventHandler OnPassesChange;
    public event EventHandler OnPassExpireDateChange;

    //---------------------------

    private Credential phonecredential;
    [SerializeField] Text errorEmail;
    [SerializeField] Text errorPassword;
    [SerializeField] Text errorLogin;
    [SerializeField] Text errorPhone;
    [SerializeField] Text errorPhoneForm;
    [SerializeField] Text errorVerificationForm;
    [SerializeField] Text errorVerificationCode;
    [SerializeField] GameObject loading;
    private bool emailFlag = false;
    private bool passwordFlag = false;
    private bool phoneFlag = false;
    private bool verificationCodeFlag = false;

    //---------------------------


    private static object _lock = new object();
    private static App _instance;
    public static App Instance
    {
        get
        {
            lock (_lock)
            {
                if (_instance == null)
                    _instance = FindObjectOfType<App>();

                return _instance;
            }
        }
    }

    void Start()
    {
        Screen.fullScreen = false;
        GoogleSignIn.Configuration = new GoogleSignInConfiguration
        {
            WebClientId = "7705776682-8b3jlj2986oujt6dm3vej2rn7qhahdpj.apps.googleusercontent.com",
            RequestEmail = true,
            RequestIdToken = true
        };
        FirebaseApp.DefaultInstance.SetEditorDatabaseUrl("https://Biletour.firebaseio.com/");
        Reference = FirebaseDatabase.DefaultInstance.RootReference;
        InitializeFirebase();
    }
    void InitializeFirebase()
    {
        Auth = FirebaseAuth.DefaultInstance;
        PhoneProvider = PhoneAuthProvider.GetInstance(Auth);
        Auth.StateChanged += AuthStateChanged;
        AuthStateChanged(this, null);
    }
    public void Buy()
    {
        PaymentHistoryList history = new PaymentHistoryList("Doładowanie konta", DateTime.Now.ToString(), "+ 5 zł");
        string json = JsonUtility.ToJson(history);
        Reference.Child(User.UserId).Child("balance").SetValueAsync(string.Format("{0:C2}", CurrentBalance + 5).Substring(1));
        Reference.Child(User.UserId).Child("paymentHistory").Child(DatetoIDConverter(history.date)).SetRawJsonValueAsync(json);
    }

    //login

    private void CheckEmail(string input)
    {
        Email = input.Trim();
        Debug.Log("email: " + Email);
        if (Email.Length > 0)
        {
            errorEmail.enabled = false;
            emailFlag = true;
        }
        else
        {
            errorEmail.enabled = true;
            emailFlag = false;
        }

    }
    private void CheckPassword(string input)
    {
        Password = input;
        Debug.Log("hasło: " + Password);
        if (Password.Length > 0)
        {
            errorPassword.enabled = false;
            passwordFlag = true;
        }
        else
        {
            errorPassword.enabled = true;
            passwordFlag = false;
        }
    }
    public void CheckEmailField(InputField input)
    {
        CheckEmail(input.text);
    }
    public void CheckPasswordField(InputField input)
    {
        CheckPassword(input.text);
    }


    public void Login()
    {
        loading.SetActive(true);
        errorLogin.enabled = false;
        CheckEmail(Email);
        CheckPassword(Password);
        if (passwordFlag == true && emailFlag == true)
        {
            Auth.SignInWithEmailAndPasswordAsync(Email, Password).ContinueWith(task =>
            {
                Debug.LogFormat("User login start");
                if (task.IsCompleted)
                {
                    FirebaseUser newUser = task.Result;
                    Debug.LogFormat("User signed in successfully: {0} ({1})", newUser.DisplayName, newUser.UserId);
                    return;
                }
                else
                {
                    Debug.LogError("SignInWithEmailAndPasswordAsync encountered an error: " + task.Exception);
                    errorLogin.enabled = true;
                    errorLogin.text = "Email lub hasło mogą być nie prawidłowe.";
                    loading.SetActive(false);
                }
            });
        }
        else
        {
            errorLogin.text = "Proszę uzupełnić pola.";
            errorLogin.enabled = true;
            loading.SetActive(false);
        }
    }

    //login with google

    public void RegisterGoogle()
    {
        Task<GoogleSignInUser> signIn = GoogleSignIn.DefaultInstance.SignIn();

        TaskCompletionSource<FirebaseUser> signInCompleted = new TaskCompletionSource<FirebaseUser>();
        signIn.ContinueWith(task =>
        {
            if (task.IsCanceled)
            {
                signInCompleted.SetCanceled();
            }
            else if (task.IsFaulted)
            {
                signInCompleted.SetException(task.Exception);
            }
            else
            {
                Credential credential = GoogleAuthProvider.GetCredential(task.Result.IdToken, null);
                Auth.SignInWithCredentialAsync(credential).ContinueWith(authTask =>
                {
                    if (authTask.IsCanceled)
                    {
                        signInCompleted.SetCanceled();
                    }
                    else if (authTask.IsFaulted)
                    {
                        signInCompleted.SetException(authTask.Exception);
                    }
                    else
                    {
                        OnGoogleLogIn?.Invoke(this, null);
                        GoogleRegisterScreen.loginflag = true;
                        signInCompleted.SetResult(authTask.Result);
                    }
                });
            }
        });
    }

    //register phone

    public void CheckPhone(InputField input)
    {
        Phone = input.text.Trim();
        if (Phone.Length > 0)
        {
            Phone = "+48" + Phone;
            errorPhone.enabled = false;
            phoneFlag = true;
        }
        else
        {
            errorPhone.enabled = true;
            phoneFlag = false;
        }
        Debug.Log("telefon: " + Phone);
    }

    public void CheckCerificationCode(InputField input)
    {
        VerificationCode = input.text.Trim();
        if (VerificationCode.Length > 0)
        {
            errorVerificationCode.enabled = false;
            verificationCodeFlag = true;
        }
        else
        {
            errorVerificationCode.enabled = true;
            verificationCodeFlag = false;
        }
        Debug.Log("verificationCode: " + VerificationCode);
    }
    public void ReadyUpRegister()
    {
        loading.SetActive(true);
        errorPhoneForm.enabled = false;
        if (phoneFlag == true)
        {
            loading.SetActive(true);
            PhoneProvider.VerifyPhoneNumber(Phone, 180000, null,
                    verificationCompleted: (credential) =>
                    {
                        // Auto-sms-retrieval or instant validation has succeeded (Android only).
                        // There is no need to input the verification code.
                        // `credential` can be used instead of calling GetCredential().
                    },
                    verificationFailed: (error) =>
                    {
                        Debug.LogError("error: " + error);
                        errorPhoneForm.text = "Nie można użyć tego numeru";
                        errorPhoneForm.enabled = true;
                        // The verification code was not sent.
                        // `error` contains a human readable explanation of the problem.
                    },
                    codeSent: (id, token) =>
                    {
                        Debug.Log("verificationId: " + id + " ; " + token);
                        VerificationId = id;
                        OnReadyUpPhoneRegister?.Invoke(this, null);
                        // Verification code was successfully sent via SMS.
                        // `id` contains the verification id that will need to passed in with
                        // the code from the user when calling GetCredential().
                        // `token` can be used if the user requests the code be sent again, to
                        // tie the two requests together.
                    },
                    codeAutoRetrievalTimeOut: (id) =>
                    {
                        Debug.Log("timeout: " + id);
                        // Called when the auto-sms-retrieval has timed out, based on the given
                        // timeout parameter.
                        // `id` contains the verification id of the request that timed out.
                    }
                );
        }
        else
        {
            errorPhoneForm.text = "Uzupełnij pole";
            errorPhoneForm.enabled = true;
        }
        loading.SetActive(false);
    }
    public void RegisterPhone()
    {
        loading.SetActive(true);
        errorVerificationForm.enabled = false;
        if (verificationCodeFlag == true)
        {
            phonecredential = PhoneProvider.GetCredential(VerificationId, VerificationCode);
            Auth.CurrentUser.LinkWithCredentialAsync(phonecredential).ContinueWith(task =>
            {
                if (task.IsFaulted)
                {
                    Debug.LogError("SignInWithCredentialAsync encountered an error: " + task.Exception);
                    errorVerificationForm.text = "Nie można użyć tego numeru";
                    errorVerificationForm.enabled = true;
                    return;
                }
                FirebaseUser newUser = task.Result;
                Debug.Log("User signed in successfully");
                // This should display the phone number.
                Debug.Log("Phone number: " + newUser.PhoneNumber);
                // The phone number providerID is 'phone'.
                Debug.Log("Phone provider ID: " + newUser.ProviderId);
            });
        }
        else
        {
            errorVerificationForm.text = "Uzupełnij pole";
            errorVerificationForm.enabled = true;
        }
        loading.SetActive(false);
    }

    void AuthStateChanged(object sender, EventArgs eventArgs)
    {
        if (Auth.CurrentUser != User)
        {
            bool signedIn = User != Auth.CurrentUser && Auth.CurrentUser != null;
            if (!signedIn && User != null)
            {
                Debug.Log("Signed out " + User.UserId);
            }
            User = Auth.CurrentUser;
            if (signedIn)
            {
                GetData();
                if (User.PhoneNumber.Length < 9)
                {
                    OnGoogleLogIn?.Invoke(this, null);
                }
                else
                {
                    Debug.Log("Signed in " + User.UserId);
                    OnLoggedIn?.Invoke(this, null);
                }
            }
        }
    }
    void GetData()
    {
        loading.SetActive(true);
        FirebaseDatabase.DefaultInstance.GetReference(User.UserId).GetValueAsync().ContinueWith(task =>
        {
            if (task.IsFaulted)
            {
                Debug.LogError("GetData Error!");
            }
            else if (task.IsCompleted)
            {
                Snapshot = task.Result;
                if (Snapshot.Child("balance").Value == null)
                {
                    Reference.Child(User.UserId).Child("balance").SetValueAsync(CurrentBalance);
                }
                else
                {
                    double.TryParse(Snapshot.Child("balance").Value.ToString(), out double currentBalance);
                    CurrentBalance = currentBalance;
                }
                FirebaseDatabase.DefaultInstance.GetReference(User.UserId + "/balance").ValueChanged += HandleBalanceValueChanged;

                if (Snapshot.Child("currentGate").Value == null)
                {
                    Reference.Child(User.UserId).Child("currentGate").SetValueAsync(LastgateID);
                }
                else
                {
                    int.TryParse(Snapshot.Child("currentGate").Value.ToString(), out int nlastgateID);
                    LastgateID = nlastgateID;
                }
                FirebaseDatabase.DefaultInstance.GetReference(User.UserId + "/currentGate").ValueChanged += HandleLastgateValueChanged;

                if (Snapshot.Child("SKMPass").Value == null)
                {
                    Reference.Child(User.UserId).Child("SKMPass").SetValueAsync(DateTime.Now.Ticks);
                }
                else
                {
                    long.TryParse(Snapshot.Child("SKMPass").Value.ToString(), out long passExpireDate);
                    PassExpireDate = new DateTime(passExpireDate);
                }
                FirebaseDatabase.DefaultInstance.GetReference(User.UserId + "/SKMPass").ValueChanged += HandlePassExpireDateValueChanged;

                foreach (var gate in Snapshot.Child("gateHistory").Children)
                {
                    gates.Add(new SKMGatesList(gate.Child("peron").Value.ToString(), IDtoDateConverter(gate.Key.ToString()), gate.Child("action").Value.ToString()));
                }
                FirebaseDatabase.DefaultInstance.GetReference(User.UserId + "/gateHistory").ValueChanged += HandleGatesChanged;

                foreach (var position in Snapshot.Child("paymentHistory").Children)
                {
                    payhistory.Add(new PaymentHistoryList(position.Child("title").Value.ToString(), IDtoDateConverter(position.Key.ToString()), position.Child("action").Value.ToString()));
                }
                FirebaseDatabase.DefaultInstance.GetReference(User.UserId + "/paymentHistory").ValueChanged += HandlePayHistoryChanged;

            }
        });

        FirebaseDatabase.DefaultInstance.GetReference("App").GetValueAsync().ContinueWith(task =>
        {
            if (task.IsFaulted)
            {
                Debug.LogError("GetData ErrorApp!");
            }
            else if (task.IsCompleted)
            {
                AppSnapshot = task.Result;

                foreach (var position in AppSnapshot.Child("SKMPass").Children)
                {
                    int.TryParse(position.Child("days").Value.ToString(), out int days);
                    List<SKMPassList> prices = new List<SKMPassList>();
                    foreach (var price in position.Child("prices").Children)
                    {
                        double.TryParse(price.Value.ToString(), out double pricing);
                        prices.Add(new SKMPassList(price.Key.ToString(), pricing));
                    }
                    passes.Add(new SKMPassList(position.Key.ToString(), days, prices));
                }
                FirebaseDatabase.DefaultInstance.GetReference("App").ValueChanged += HandleAppChanged;
            }
        });

        loading.SetActive(false);
    }

    private void HandlePassExpireDateValueChanged(object sender, ValueChangedEventArgs args)
    {
        //throw new NotImplementedException();
        if (args.DatabaseError != null)
        {
            Debug.LogError(args.DatabaseError.Message);
            return;
        }
        // Do something with the data in args.Snapshot
        long.TryParse(args.Snapshot.Value.ToString(), out long passExpireDate);
        PassExpireDate = new DateTime(passExpireDate);
        OnPassExpireDateChange?.Invoke(this, null);
    }

    public void HandleBalanceValueChanged(object sender, ValueChangedEventArgs args)
    {
        if (args.DatabaseError != null)
        {
            Debug.LogError(args.DatabaseError.Message);
            return;
        }
        // Do something with the data in args.Snapshot
        double.TryParse(args.Snapshot.Value.ToString(), out double currentBalance);
        CurrentBalance = currentBalance;
        OnBalanceChange?.Invoke(this, null);
    }
    public void HandleLastgateValueChanged(object sender, ValueChangedEventArgs args)
    {
        if (args.DatabaseError != null)
        {
            Debug.LogError(args.DatabaseError.Message);
            return;
        }
        // Do something with the data in args.Snapshot
        int.TryParse(args.Snapshot.Value.ToString(), out int nlastgateID);
        LastgateID = nlastgateID;
    }

    public void HandleGatesChanged(object sender, ValueChangedEventArgs args)
    {
        if (args.DatabaseError != null)
        {
            Debug.LogError(args.DatabaseError.Message);
            return;
        }
        // Do something with the data in args.Snapshot

        gates = new List<SKMGatesList>();
        foreach (var gate in args.Snapshot.Children)
        {
            gates.Add(new SKMGatesList(gate.Child("peron").Value.ToString(), IDtoDateConverter(gate.Key.ToString()), gate.Child("action").Value.ToString()));
        }
        OnNewGatesHistory?.Invoke(this, null);
    }

    private void HandlePayHistoryChanged(object sender, ValueChangedEventArgs args)
    {
        if (args.DatabaseError != null)
        {
            Debug.LogError(args.DatabaseError.Message);
            return;
        }
        // Do something with the data in args.Snapshot

        payhistory = new List<PaymentHistoryList>();
        foreach (var position in args.Snapshot.Children)
        {
            payhistory.Add(new PaymentHistoryList(position.Child("title").Value.ToString(), IDtoDateConverter(position.Key.ToString()), position.Child("action").Value.ToString()));
        }
        OnNewHistoryInput?.Invoke(this, null);
    }

    public void HandleAppChanged(object sender, ValueChangedEventArgs args)
    {
        AppSnapshot = args.Snapshot;
        OnPassesChange?.Invoke(this, null);
    }
    public static string IDtoDateConverter(string ID)
    {
        var fdateid = new StringBuilder();
        int i = 0, j = 0;

        foreach (var c in ID)
        {
            i++;
            fdateid.Append(c);
            if ((++j % 2) == 0)
            {
                if (i < 5)
                    fdateid.Append('/');
                else if (i == 8)
                    fdateid.Append(' ');
                else if (i > 9 && i < 13)
                    fdateid.Append(':');
            }
        }

        return fdateid.ToString();
    }

    public static string DatetoIDConverter(string date)
    {
        var fdateid = new StringBuilder();

        foreach (var c in date)
        {
            if (c != '/' && c != ' ' && c != ':')
            {
                fdateid.Append(c);
            }
        }

        return fdateid.ToString();
    }
}
