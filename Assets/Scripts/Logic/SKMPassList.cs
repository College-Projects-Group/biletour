﻿using System.Collections.Generic;
using UnityEngine;

public class SKMPassList : MonoBehaviour
{
    public string pass;
    public int days;
    public string type;
    public double price;
    public List<SKMPassList> prices;

    public SKMPassList(string newType, double newPrice)
    {
        type = newType;
        price = newPrice;
    }

    public SKMPassList(string newPass, int newDays, List<SKMPassList> newPrices)
    {
        pass = newPass;
        days = newDays;
        prices = newPrices;
    }

}
