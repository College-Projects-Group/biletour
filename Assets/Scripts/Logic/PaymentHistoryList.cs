﻿using UnityEngine;

public class PaymentHistoryList : MonoBehaviour
{
    public string title;
    public string date;
    public string action;

    public PaymentHistoryList(string newTitle, string newDate, string newAction)
    {
        title = newTitle;
        date = newDate;
        action = newAction;
    }
}
