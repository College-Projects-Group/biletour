﻿using UnityEngine;
using Firebase.Auth;
using UnityEngine.UI;
using System;

public class LoginScreen : Page
{
    [SerializeField] Text errorEmail;
    [SerializeField] Text errorPassword;
    [SerializeField] Text errorLogin;
    [SerializeField] GameObject loading;

    public override void Load()
    {
        errorEmail.enabled = false;
        errorPassword.enabled = false;
        errorLogin.enabled = false;
        loading.SetActive(false);
        App.Instance.OnLoggedIn += OnLoggedInHandler;
    }

    private void OnLoggedInHandler(object sender, EventArgs e)
    {
        App.Instance.OnLoggedIn -= OnLoggedInHandler;
        SwitchPage("MainMenu");
    }

    public override void Return()
    {
        FirebaseAuth.DefaultInstance.SignOut();
        SwitchPage("WelcomeScreen");
    }

    public override void UnLoad()
    {
        App.Password = null;
    }
}
