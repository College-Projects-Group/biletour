﻿public class AlertValidGate : Page
{
    public override void Load()
    {
    }

    public override void UnLoad()
    {
    }

    public override void Return()
    {
    }
    public void CloseAlertValidGate()
    {
        NFCPlugin.enableNFCScan();
        gameObject.SetActive(false);
        UnLoad();
    }
}
