﻿public class AlertNoFunds : Page
{
    public override void Load()
    {
    }

    public override void Return()
    {
    }

    public override void UnLoad()
    {
    }

    public void CloseAlertNoFunds()
    {
        NFCPlugin.enableNFCScan();
        gameObject.SetActive(false);
        UnLoad();
    }
}
