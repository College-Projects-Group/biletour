﻿using Firebase.Database;
using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SKMBuyPass : Page
{
    [SerializeField] Dropdown tickets;
    [SerializeField] Dropdown prices;
    [SerializeField] Text price;
    [SerializeField] Page AlertNoFunds;
    public override void Load()
    {
        App.Instance.OnPassesChange += HandlePassChanged;

        FillOptions(App.passes);
        tickets.onValueChanged.AddListener(delegate
        {
            HandleTicketChange();
        });

        SetDiscounts(App.passes[tickets.value]);
        prices.onValueChanged.AddListener(delegate
        {
            HandleDiscountChange();
        });

        WritePrice(App.passes[tickets.value].prices[prices.value].price);
    }

    private void FillOptions(List<SKMPassList> passes)
    {
        tickets.ClearOptions();
        foreach (var pass in passes)
        {
            tickets.options.Add(new Dropdown.OptionData() { text = pass.pass });

        }
        tickets.RefreshShownValue();
    }

    private void SetDiscounts(SKMPassList pass)
    {
        prices.ClearOptions();
        foreach (var price in pass.prices)
        {
            prices.options.Add(new Dropdown.OptionData() { text = price.type });
        }
        prices.RefreshShownValue();
    }

    private void HandlePassChanged(object sender, EventArgs e)
    {
        FillOptions(App.passes);
    }

    private void HandleTicketChange()
    {
        SetDiscounts(App.passes[tickets.value]);
        HandleDiscountChange();
    }
    private void HandleDiscountChange()
    {
        WritePrice(App.passes[tickets.value].prices[prices.value].price);
    }

    private void WritePrice(double price)
    {
        this.price.text = string.Format("{0:C2} zł", price).Substring(1);
    }

    public override void Return()
    {
        SwitchPage("SKMGates");
    }

    public override void UnLoad()
    {
        tickets.onValueChanged.RemoveAllListeners();
    }

    public void BuyPass()
    {
        var price = App.passes[tickets.value].prices[prices.value].price;
        if (App.CurrentBalance >= price)
        {
            var date = DateTime.Now;
            PaymentHistoryList history = new PaymentHistoryList("Zakup przepustki SKM - " + App.passes[tickets.value].pass, date.ToString(), "- " + string.Format("{0:C2} zł", price).Substring(1));
            string jsonhistory = JsonUtility.ToJson(history);

            App.Reference.Child(App.User.UserId).Child("paymentHistory").Child(App.DatetoIDConverter(date.ToString())).SetRawJsonValueAsync(jsonhistory);
            App.Reference.Child(App.User.UserId).Child("balance").SetValueAsync(string.Format("{0:C2}", App.CurrentBalance - price).Substring(1));
            if(App.PassExpireDate < date)
                App.Reference.Child(App.User.UserId).Child("SKMPass").SetValueAsync(date.AddDays(App.passes[tickets.value].days).Ticks);
            else
                App.Reference.Child(App.User.UserId).Child("SKMPass").SetValueAsync(App.PassExpireDate.AddDays(App.passes[tickets.value].days).Ticks);

            SwitchPage("SKMGates");
        }
        else
        {
            AlertNoFunds.Load();
            AlertNoFunds.gameObject.SetActive(true);
            return;
        }
    }
}