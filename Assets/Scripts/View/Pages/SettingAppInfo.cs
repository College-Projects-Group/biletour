﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SettingAppInfo : Page
{

    public override void Load()
    {
    }

    public override void Return()
    {
        SwitchPage("Settings");
    }

    public override void UnLoad()
    {
    }

    public void GotoTerms()
    {
        SwitchPage("SettingsAppInfoTerms");
    }
    public void GotoLicenses()
    {
        SwitchPage("SettingsAppInfoLicences");
    }
}
