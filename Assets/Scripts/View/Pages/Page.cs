﻿using UnityEngine;

public abstract class Page : MonoBehaviour
{

    public delegate void ChangePage(string nextpage);

    public event ChangePage OnChangePage;
    public abstract void Load();

    public abstract void UnLoad();

    public abstract void Return();

    protected void SwitchPage(string pageName)
    {
        OnChangePage?.Invoke(pageName);
    }
}
