﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SettingsPaymentHistory : Page
{
    [SerializeField] Component PrefabGate;
    [SerializeField] GameObject ViewContent;

    public override void Load()
    {
        DrawList(App.payhistory);
        App.Instance.OnNewHistoryInput += OnNewHistoryInputHandler;
    }

    public void DrawList(List<PaymentHistoryList> position)
    {
        position.Reverse();
        foreach (Transform child in ViewContent.transform)
        {
            Destroy(child.gameObject);
        }
        foreach (PaymentHistoryList x in position)
        {
            var transaction = Instantiate(PrefabGate, ViewContent.transform);
            transaction.GetComponentsInChildren<Text>()[0].text = x.title;
            transaction.GetComponentsInChildren<Text>()[1].text = x.date;
            transaction.GetComponentsInChildren<Text>()[2].text = x.action;
        }
        position.Reverse();
    }

    private void OnNewHistoryInputHandler(object sender, EventArgs e)
    {
        DrawList(App.payhistory);
    }

    public override void Return()
    {
        SwitchPage("Settings");
    }

    public override void UnLoad()
    {
        App.Instance.OnNewHistoryInput -= OnNewHistoryInputHandler;
    }
}
