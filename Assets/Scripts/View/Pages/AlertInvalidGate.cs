﻿public class AlertInvalidGate : Page
{
    public override void Load()
    {
    }

    public override void Return()
    {
    }

    public override void UnLoad()
    {
    }

    public void CloseAlertInvalidGate()
    {
        NFCPlugin.enableNFCScan();
        gameObject.SetActive(false);
        UnLoad();
    }
}
