﻿using Firebase.Auth;
using Google;
using System;
using UnityEngine;

public class WelcomeScreen : Page
{
    public void Login()
    {
        SwitchPage("LoginScreen");
    }

    public void Register()
    {
        SwitchPage("RegisterScreen");
    }

    public void GoogleLogin()
    {
        SwitchPage("GoogleRegisterScreen");
    }
    public void GotoTerms()
    {
        SwitchPage("SettingsAppInfoTerms");
    }
    public override void Load()
    {
        App.Instance.OnLoggedIn += OnLoggedInHandler;
        App.Instance.OnGoogleLogIn += OnLoggedInHandler;
    }

    private void OnLoggedInHandler(object sender, EventArgs e)
    {
        App.Instance.OnLoggedIn -= OnLoggedInHandler;
        Debug.Log("ProviderIDtest: " + App.User.ProviderData);
        if (App.User.Email.Length < 1)
            SwitchPage("RegisterScreen");
        else if (App.User.PhoneNumber.Length < 1)
            SwitchPage("GoogleRegisterScreen");
        else
            SwitchPage("MainMenu");
    }
    public override void Return()
    {
        Application.Quit();
    }

    public override void UnLoad()
    {
        App.Instance.OnLoggedIn -= OnLoggedInHandler;
        App.Instance.OnGoogleLogIn -= OnLoggedInHandler;
    }
}
