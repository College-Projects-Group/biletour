﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Firebase.Auth;
using Google;

public class Settings : Page
{

    public void GotoSettingsAccount()
    {
        SwitchPage("SettingsAccount");
    }

    public void GotoSettingsPaymentMethods()
    {
        SwitchPage("SettingsPaymentMethods");
    }

    public void GotoSettingsPaymentHistory()
    {
        SwitchPage("SettingsPaymentHistory");
    }

    public void GotoSettingsAppInfo()
    {
        SwitchPage("SettingsAppInfo");
    }

    public override void Load()
    {
    }

    public override void Return()
    {
        SwitchPage("MainMenu");
    }

    public override void UnLoad()
    {
    }
}
