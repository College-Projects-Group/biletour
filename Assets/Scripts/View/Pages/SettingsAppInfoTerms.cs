﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SettingsAppInfoTerms : Page
{
    public override void Load()
    {
    }

    public override void Return()
    {
        if (App.User == null)
            SwitchPage("WelcomeScreen");
        else
            SwitchPage("SettingsAppInfo");
    }

    public override void UnLoad()
    {
    }
}
