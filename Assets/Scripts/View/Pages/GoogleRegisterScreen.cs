﻿using UnityEngine;
using Firebase.Auth;
using UnityEngine.UI;
using Google;
using System;

public class GoogleRegisterScreen : Page
{
    [SerializeField] Text pageName;
    [SerializeField] Text errorPhone;
    [SerializeField] Text errorPhoneForm;
    [SerializeField] Text errorVerificationForm;
    [SerializeField] Text errorVerificationCode;
    [SerializeField] Text button;
    [SerializeField] GameObject loading;
    [SerializeField] GameObject PhoneForm;
    [SerializeField] GameObject SmsVerification;
    public static bool loginflag = false;

    public override void Load()
    {
        loading.SetActive(true);
        SmsVerification.SetActive(false);
        PhoneForm.SetActive(false);
        pageName.enabled = false;
        errorPhone.enabled = false;
        errorPhoneForm.enabled = false;
        errorVerificationForm.enabled = false;
        errorVerificationCode.enabled = false;
        if (App.User == null)
            App.Instance.OnGoogleLogIn += OnGoogleLoggedInHandler;
        else
        {
            button.text = "Wyloguj i Zamknij";
            StartForm();
        }
    }

    public override void Return()
    {
        if (App.User == null)
            SwitchPage("WelcomeScreen");
        else
        {
            FirebaseAuth.DefaultInstance.SignOut();
            GoogleSignIn.DefaultInstance.SignOut();
            GoogleSignIn.DefaultInstance.Disconnect();
            Application.Quit();
        }
    }

    public override void UnLoad()
    {
        App.Instance.OnGoogleLogIn -= OnGoogleLoggedInHandler;
    }

    private void OnGoogleLoggedInHandler(object sender, EventArgs e)
    {
        button.text = "Wyloguj i Zamknij";
        StartForm();
    }
    private void StartForm()
    {
        App.Instance.OnReadyUpPhoneRegister += OnReadyUpPhoneRegisterHandler;
        pageName.enabled = true;
        PhoneForm.SetActive(true);
        loading.SetActive(false);
    }
    private void OnReadyUpPhoneRegisterHandler(object sender, EventArgs e)
    {
        App.Instance.OnReadyUpPhoneRegister -= OnReadyUpPhoneRegisterHandler;
        PhoneForm.SetActive(false);
        SmsVerification.SetActive(true);
    }

    // Update is called once per frame
    void Update()
    {
        if (loginflag == true && App.User.PhoneNumber.Length >= 9)
        {
            loginflag = false;
            Debug.Log("Loggedin with update on google register screen");
            SwitchPage("MainMenu");
        }
    }
}
