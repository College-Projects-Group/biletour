﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public partial class GUIManager : MonoBehaviour
{
    [SerializeField] private Page currentPage;

    private Page nextPage;
    private Dictionary<string, Page> pages;


    private void Awake()
    {
        pages = new Dictionary<string, Page>();
        var foundPages = GetComponentsInChildren<Page>(true);

        foreach (var page in foundPages)
        {
            if (!pages.ContainsKey(page.gameObject.name))
            {
                pages.Add(page.gameObject.name, page);
                page.gameObject.SetActive(false);
            }

        }

        currentPage.gameObject.SetActive(true);
    }

    void Start()
    {
        currentPage.OnChangePage += ChangePage;
        currentPage.Load();
    }

    private void Update()
    {
    #if UNITY_ANDROID
        if (Input.GetKeyUp(KeyCode.Escape))
            currentPage.Return();

    #endif
    }

    public void ChangePage(string pageName)
    {
        nextPage = FindPage(pageName);
        currentPage.OnChangePage -= ChangePage;
        currentPage.gameObject.SetActive(false);
        currentPage.UnLoad();
        currentPage = nextPage;
        currentPage.OnChangePage += ChangePage;
        currentPage.Load();
        currentPage.gameObject.SetActive(true);
    }
    public Page FindPage(string name)
    {
        if (!pages.ContainsKey(name))
            throw new System.ArgumentNullException("name", "strona " + name + " nie istnieje");

        return pages[name];
    }

}