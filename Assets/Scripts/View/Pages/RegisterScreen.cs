﻿using UnityEngine;
using Firebase.Auth;
using UnityEngine.UI;
using System.Text.RegularExpressions;

public class RegisterScreen : Page
{
    [SerializeField] Text errorEmail;
    [SerializeField] Text errorPhone;
    [SerializeField] Text errorPassword;
    [SerializeField] Text errorPasswordConfirm;
    [SerializeField] Text errorPhoneForm;
    [SerializeField] Text errorVerificationForm;
    [SerializeField] Text errorEmailForm;
    [SerializeField] Text errorVerificationCode;
    [SerializeField] Text button;
    [SerializeField] GameObject loading;
    [SerializeField] GameObject PhoneForm;
    [SerializeField] GameObject Form;
    [SerializeField] GameObject SmsVerification;
    private bool loginflag = false;
    private bool emailFlag = false;
    private bool phoneFlag = false;
    private bool passwordFlag = false;
    private bool passwordconfirmFlag = false;
    private bool verificationCodeFlag = false;
    private Credential emailcredential;
    private Credential phonecredential;
    private const string MatchEmailPattern =
        @"^(([\w-]+\.)+[\w-]+|([a-zA-Z]{1}|[\w-]{2,}))@"
        + @"((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\.([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\."
        + @"([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\.([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|"
        + @"([a-zA-Z]+[\w-]+\.)+[a-zA-Z]{2,4})$";

    public void CheckEmail(InputField input)
    {
        if (input.text != null)
        {
            App.Email = input.text.Trim();
            Debug.Log("email: " + App.Email + "; " + Regex.IsMatch(App.Email, MatchEmailPattern));
            if (Regex.IsMatch(App.Email, MatchEmailPattern))
            {
                errorEmail.enabled = false;
                emailFlag = true;
            }
            else
            {
                errorEmail.enabled = true;
                emailFlag = false;
            }
        }
    }
    public void CheckPhone(InputField input)
    {
        App.Phone = input.text.Trim();
        if (App.Phone.Length > 0)
        {
            App.Phone = "+48" + App.Phone;
            errorPhone.enabled = false;
            phoneFlag = true;
        }
        else
        {
            errorPhone.enabled = true;
            phoneFlag = false;
        }
        Debug.Log("telefon: " + App.Phone);
    }

    public void CheckPassword(InputField input)
    {
        App.Password = input.text;
        Debug.Log("hasło: " + App.Password);
        if (App.Password.Length > 5)
        {
            errorPassword.enabled = false;
            passwordFlag = true;
        }
        else
        {
            errorPassword.enabled = true;
            passwordFlag = false;
        }
        if (App.Password != App.Passwordconfirm)
        {
            errorPasswordConfirm.enabled = true;
            passwordconfirmFlag = false;
        }
    }
    public void CheckPasswordConfirm(InputField input)
    {
        App.Passwordconfirm = input.text;
        Debug.Log("passwordconfirm: " + App.Passwordconfirm);
        if (App.Password == App.Passwordconfirm)
        {
            errorPasswordConfirm.enabled = false;
            passwordconfirmFlag = true;
        }
        else
        {
            errorPasswordConfirm.enabled = true;
            passwordconfirmFlag = false;
        }
    }
    public void CheckCerificationCode(InputField input)
    {
        App.VerificationCode = input.text.Trim();
        if (App.VerificationCode.Length > 0)
        {
            errorVerificationCode.enabled = false;
            verificationCodeFlag = true;
        }
        else
        {
            errorVerificationCode.enabled = true;
            verificationCodeFlag = false;
        }
        Debug.Log("verificationCode: " + App.VerificationCode);
    }
    public void ReadyUpRegister()
    {
        errorPhoneForm.enabled = false;
        if (phoneFlag == true)
        {
            loading.SetActive(true);
            App.PhoneProvider.VerifyPhoneNumber(App.Phone, 180000, null,
                    verificationCompleted: (credential) =>
                    {
                        // Auto-sms-retrieval or instant validation has succeeded (Android only).
                        // There is no need to input the verification code.
                        // `credential` can be used instead of calling GetCredential().
                    },
                    verificationFailed: (error) =>
                    {
                        Debug.LogError("error: " + error);
                        // The verification code was not sent.
                        // `error` contains a human readable explanation of the problem.
                    },
                    codeSent: (id, token) =>
                    {
                        Debug.Log("verificationId: " + id + " ; " + token);
                        App.VerificationId = id;
                        PhoneForm.SetActive(false);
                        SmsVerification.SetActive(true);
                        loading.SetActive(false);
                        // Verification code was successfully sent via SMS.
                        // `id` contains the verification id that will need to passed in with
                        // the code from the user when calling GetCredential().
                        // `token` can be used if the user requests the code be sent again, to
                        // tie the two requests together.
                    },
                    codeAutoRetrievalTimeOut: (id) =>
                    {
                        Debug.Log("timeout: " + id);
                        // Called when the auto-sms-retrieval has timed out, based on the given
                        // timeout parameter.
                        // `id` contains the verification id of the request that timed out.
                    }
                );
        }
        else
        {
            errorPhoneForm.enabled = true;
        }
    }

    public void RegisterPhone()
    {
        errorVerificationForm.enabled = false;
        if (verificationCodeFlag == true)
        {
            loading.SetActive(true);
            phonecredential = App.PhoneProvider.GetCredential(App.VerificationId, App.VerificationCode);
            App.Auth.SignInWithCredentialAsync(phonecredential).ContinueWith(task =>
            {
                if (task.IsFaulted)
                {
                    Debug.LogError("SignInWithCredentialAsync encountered an error: " + task.Exception);
                    return;
                }
                FirebaseUser newUser = task.Result;
                Debug.Log("User signed in successfully");
                // This should display the Phone number.
                Debug.Log("App.Phone number: " + newUser.PhoneNumber);
                // The Phone number providerID is 'phone'.
                Debug.Log("App.Phone provider ID: " + newUser.ProviderId);
            });
            SmsVerification.SetActive(false);
            Form.SetActive(true);
            loading.SetActive(false);
        }
        else
        {
            errorVerificationForm.enabled = true;
        }
    }
    public void RegisterEmail()
    {
        errorEmailForm.enabled = false;
        if (emailFlag == true && passwordFlag == true && passwordconfirmFlag == true)
        {
            loading.SetActive(true);
            emailcredential = EmailAuthProvider.GetCredential(App.Email, App.Password);
            App.Auth.CurrentUser.LinkWithCredentialAsync(emailcredential).ContinueWith(task =>
            {
                if (task.IsFaulted)
                {
                    errorEmailForm.text = "Wystąpił błąd rejestracji";
                    errorEmailForm.enabled = true;
                    Debug.LogError("LinkWithCredentialAsync encountered an error: " + task.Exception);
                    return;
                }
                button.text = "Wyloguj i Zamknij";
                loginflag = true;
                FirebaseUser newUser = task.Result;
                Debug.LogFormat("Credentials successfully linked to Firebase user: {0} ({1})", newUser.Email, newUser.UserId);
            });
            loading.SetActive(false);
        }
        else
        {
            errorEmailForm.text = "Uzupełnij pola";
            errorEmailForm.enabled = true;
        }
    }

    public override void Load()
    {
        errorEmail.enabled = false;
        errorPhone.enabled = false;
        errorPassword.enabled = false;
        errorPasswordConfirm.enabled = false;
        errorPhoneForm.enabled = false;
        errorVerificationForm.enabled = false;
        errorEmailForm.enabled = false;
        errorVerificationCode.enabled = false;
        loading.SetActive(false);
        if (App.User == null)
        {
            SmsVerification.SetActive(false);
            Form.SetActive(false);
            PhoneForm.SetActive(true);
        }
        else
        {
            button.text = "Wyloguj i Zamknij";
            SmsVerification.SetActive(false);
            PhoneForm.SetActive(false);
            Form.SetActive(true);
        }
    }

    public override void Return()
    {
        if (App.User == null)
            SwitchPage("WelcomeScreen");
        else
        {
            FirebaseAuth.DefaultInstance.SignOut();
            Application.Quit();
        }
    }

    public override void UnLoad()
    {
        App.Password = "";
        App.Passwordconfirm = "";
    }

    // Update is called once per frame
    void Update()
    {
        if (loginflag == true && App.User.Email.Length >= 1)
        {
            loginflag = false;
            Debug.Log("Loggedin with update on email register screen");
            SwitchPage("MainMenu");
        }
    }
}
