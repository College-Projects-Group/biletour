﻿using UnityEngine;
using UnityEngine.UI;
using Firebase.Auth;
using Google;
using System;

public class SettingsAccount : Page
{
    [SerializeField] private Text email;
    [SerializeField] private Text phone;
    [SerializeField] private Text balance;

    public void Logout()
    {
        FirebaseAuth.DefaultInstance.SignOut();
        GoogleSignIn.DefaultInstance.SignOut();
        GoogleSignIn.DefaultInstance.Disconnect();
        Application.Quit();
    }

    public override void Load()
    {
        balance.text = string.Format("{0:C2} zł", App.CurrentBalance).Substring(1);
        App.Instance.OnBalanceChange += OnBalanceChangeHandler;
        email.text = App.User.Email;
        phone.text = App.User.PhoneNumber.Substring(3);
    }

    public override void Return()
    {
        SwitchPage("Settings");
    }

    public override void UnLoad()
    {
        App.Instance.OnBalanceChange -= OnBalanceChangeHandler;
    }

    private void OnBalanceChangeHandler(object sender, EventArgs e)
    {
        balance.text = string.Format("{0:C2} zł", App.CurrentBalance).Substring(1);
    }
}
