﻿using System;
using UnityEngine;
using UnityEngine.UI;

public class MainMenu : Page
{
    [SerializeField] Text balance;
    [SerializeField] GameObject loading;

    public void GotoBuyTicketsMenu()
    {
        SwitchPage("BuyTickets");
    }

    public void GotoActiveTicketsMenu()
    {
        SwitchPage("ActiveTickets");
    }

    public void GotoSettings()
    {
        SwitchPage("Settings");
    }
    public void GotoSKMGates()
    {
        SwitchPage("SKMGates");
    }

    public void GotoFindRoute()
    {
        SwitchPage("FindRoute");
    }
    public override void Load()
    {
        if(double.IsNaN(App.CurrentBalance))
            balance.text = string.Format("{0:C2} zł", App.CurrentBalance).Substring(1);
        App.Instance.OnBalanceChange += OnBalanceChangeHandler;
    }

    public override void Return()
    {
        Application.Quit();
    }

    public override void UnLoad()
    {
        App.Instance.OnBalanceChange += OnBalanceChangeHandler;
    }

    void Start()
    {
        loading.SetActive(false);
        NFCPlugin.enableNFCScan();
        Debug.Log("enabled NFC data");
    }

    private void OnBalanceChangeHandler(object sender, EventArgs e)
    {
        balance.text = string.Format("{0:C2} zł", App.CurrentBalance).Substring(1);
    }

}
