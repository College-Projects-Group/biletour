﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SKMGates : Page
{
    [SerializeField] Component PrefabGate;
    [SerializeField] GameObject ViewContent;
    [SerializeField] Text accStatus;
    public void GotoSKMBuyPass()
    {
        SwitchPage("SKMBuyPass");
    }
    public override void Load()
    {
        if (App.PassExpireDate > DateTime.Now)
            accStatus.text = App.PassExpireDate.ToString();
        else
            accStatus.text = "Brak Aktywnego biletu okresowego";
        App.Instance.OnPassExpireDateChange += OnPassExpireDateChangeHandler;

        DrawList(App.gates);
        App.Instance.OnNewGatesHistory += OnNewGatesHistoryHandler;
    }

    public override void Return()
    {
        SwitchPage("MainMenu");
    }

    public override void UnLoad()
    {
        App.Instance.OnNewGatesHistory -= OnNewGatesHistoryHandler;
    }

    // Update is called once per frame
    void Update()
    {
        if (App.PassExpireDate < DateTime.Now)
            accStatus.text = "Brak Aktywnego biletu okresowego";
    }

    private void OnPassExpireDateChangeHandler(object sender, EventArgs e)
    {
        accStatus.text = App.PassExpireDate.ToString();
    }

    private void OnNewGatesHistoryHandler(object sender, EventArgs e)
    {
        DrawList(App.gates);
    }

    public void DrawList(List<SKMGatesList> gates)
    {
        gates.Reverse();
        foreach (Transform child in ViewContent.transform)
        {
            Destroy(child.gameObject);
        }
        foreach (SKMGatesList x in gates)
        {
            var gate = Instantiate(PrefabGate, ViewContent.transform);
            gate.GetComponentsInChildren<Text>()[0].text = x.peron;
            gate.GetComponentsInChildren<Text>()[1].text = x.date;
            gate.GetComponentsInChildren<Text>()[2].text = x.action;
        }
        gates.Reverse();
    }
}
