package com.pjatk.unity.plugin;

import java.io.UnsupportedEncodingException;
import java.util.Arrays;

import android.app.PendingIntent;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.IntentFilter.MalformedMimeTypeException;
import android.nfc.NdefMessage;
import android.nfc.NdefRecord;
import android.nfc.NfcAdapter;
import android.os.Bundle;
import android.os.Parcelable;
import android.util.Log;
import android.view.WindowManager;

import com.unity3d.player.UnityPlayer;
import com.unity3d.player.UnityPlayerActivity;

public class NFCPlugin extends UnityPlayerActivity {

    private static final String LOGTAG = "NFC-PJATK";
    private boolean NFCMode = true;

    public NFCPlugin() {
        Log.i(LOGTAG,"Created CustomPlugin");
    }

    // Callback object and method
    private String gameObject;
    private String methodName;

    private NfcAdapter nfcAdapter = null;
    private PendingIntent pendingIntent;
    private IntentFilter[] filters;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setTheme(android.R.style.Theme_NoTitleBar_Fullscreen);

        // Create NFC Adapter
        nfcAdapter = NfcAdapter.getDefaultAdapter(this);

        // Check for Hardware
        if (nfcAdapter == null) {
            // Log on no NFC
            Log.i(LOGTAG, "NO_HARDWARE");
            return;
        }


        // Pending intent
        pendingIntent = PendingIntent.getActivity(this, 0,
                new Intent(this, NFCPlugin.class).addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP), 0);

        // Setup an intent filter for all MIME based dispatches (TEXT);
        IntentFilter ndefText = new IntentFilter(NfcAdapter.ACTION_NDEF_DISCOVERED);
        try {
            ndefText.addDataType("*/*");
        } catch (MalformedMimeTypeException ignored) {
        }

        filters = new IntentFilter[] { ndefText };
    }

    public void scanNFClinkup(String gameObject, String methodName) {

        // Save callback object and method
        this.gameObject = gameObject;
        this.methodName = methodName;

    }

    @Override
    public void onResume() {
        super.onResume();
        if (nfcAdapter != null && NFCMode) {
            nfcAdapter.enableForegroundDispatch(this, pendingIntent, filters, null);
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        if (nfcAdapter != null && NFCMode) {
            nfcAdapter.disableForegroundDispatch(this);
        }
    }

    //response from the scan activity (not background scan mode)
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 0) {
            if (resultCode == RESULT_OK) {
                TellUnity(data.getStringExtra("SCAN_RESULT"));
            } else {
                TellUnity("CANCELLED");
            }
        }
    }

    //entry point when scanning (only to be used when on "background mode")
    @Override
    public void onNewIntent(Intent intent) {
        super.onNewIntent(intent);

        String nfcText = "";

        Parcelable[] ndefMessages = (Parcelable[]) intent.getParcelableArrayExtra("android.nfc.extra.NDEF_MESSAGES");
        if (ndefMessages != null)
        {
            try
            {
                for (Parcelable message : ndefMessages) {
                    for (NdefRecord ndefRecord : ((NdefMessage) (message)).getRecords()) {
                        if (ndefRecord.getTnf() == NdefRecord.TNF_WELL_KNOWN &&
                                Arrays.equals(ndefRecord.getType(), NdefRecord.RTD_TEXT)) {

                            byte[] payLoad = ndefRecord.getPayload();

                            //Get the Text Encoding
                            String textEncoding = ((payLoad[0] & 128) == 0) ? "UTF-8" : "UTF-16";
                            //Get the Language Code
                            int languageCodeLength = payLoad[0] & 63;
                            //String languageCode = new String(payLoad, 1, languageCodeLength, "US-ASCII");

                            //Get the Text
                            try {
                                nfcText += new String(payLoad, languageCodeLength + 1, payLoad.length - languageCodeLength - 1, textEncoding);
                                Log.i(LOGTAG, nfcText);
                            } catch (UnsupportedEncodingException e) {
                                Log.e("UnsupportedEncoding", e.toString());
                            }
                        }
                    }
                }
            }
            catch (Exception e){
                TellUnity("ERROR - " + e);
                return;
            }
        }
        // Scan completed
        TellUnity(nfcText);
    }

    private void TellUnity(String message)
    {
        UnityPlayer.UnitySendMessage(NFCPlugin.this.gameObject, NFCPlugin.this.methodName, message);
    }

    public void NFCModeOn(){
        NFCMode = true;
        nfcAdapter.enableForegroundDispatch(this, pendingIntent, filters, null);
    }

    public void NFCModeOff(){
        NFCMode = false;
        nfcAdapter.disableForegroundDispatch(this);
    }
}